<?php $__env->startSection('content'); ?>

    <section>
        <div class="table-responsive">
            <table id="project_status-table" class="table ">
                <thead>
                <tr>
                    <th class="not-exported"></th>
                    <th><?php echo e(__('Project Summary')); ?></th>
                    <th><?php echo e(trans('file.Priority')); ?></th>
                    <th><?php echo e(__('Assigned Employees')); ?></th>
                    <th><?php echo e(__('Start Date')); ?></th>
                    <th><?php echo e(__('End Date')); ?></th>
                    <th><?php echo e(__('Project Progress')); ?></th>
                </tr>
                </thead>


                <tbody id="tablecontents">
                <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="row1" data-id="<?php echo e($project->id); ?>">
                        <td><?php echo e($key); ?></td>
                        <td><?php echo e($project->title); ?></td>
                        <td><?php echo e($project->project_priority); ?></td>
                        <td>
                            <?php
                        $assigned_name = $project->assignedEmployees()->pluck('last_name', 'first_name');
                        $collection = [];
                        foreach ($assigned_name as $first => $last)
                        {
                        $full_name = $first . ' ' . $last;
                        echo $full_name . '<br>';
                        }
                        ?>

                        </td>
                        <td><?php echo e($project->start_date); ?></td>
                        <td><?php echo e($project->end_date); ?></td>
                        <td><?php echo e($project->project_progress); ?> %</td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>



            </table>
        </div>
    </section>

    <script>
        (function($) { 
            "use strict"; 
            
            $('#project_status-table').DataTable({
                "order": [],
                'columnDefs': [
                    {
                        "orderable": false,
                        'targets': [0,3]
                    },
                    {
                        'checkboxes': {
                            'selectRow': true
                        },
                        'targets': [0]
                    }
                ],
                'select': {style: 'multi', selector: 'td:first-child'},
                'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: '<"row"lfB>rtip',
                buttons: [
                    {
                            extend: 'pdf',
                            text: '<i title="export to pdf" class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: ':visible:Not(.not-exported)',
                                rows: ':visible'
                            },
                        customize: function (doc) {
                            for (let i = 1; i < doc.content[1].table.body.length; i++) {
                                if (doc.content[1].table.body[i][0].text.indexOf('<img src=') !== -1) {
                                    let imagehtml = doc.content[1].table.body[i][0].text;
                                    let regex = /<img.*?src=['"](.*?)['"]/;
                                    let src = regex.exec(imagehtml)[1];
                                    let tempImage = new Image();
                                    tempImage.src = src;
                                    let canvas = document.createElement("canvas");
                                    canvas.width = tempImage.width;
                                    canvas.height = tempImage.height;
                                    let ctx = canvas.getContext("2d");
                                    ctx.drawImage(tempImage, 0, 0);
                                    let imagedata = canvas.toDataURL("image/png");
                                    delete doc.content[1].table.body[i][0].text;
                                    doc.content[1].table.body[i][0].image = imagedata;
                                    doc.content[1].table.body[i][0].fit = [30, 30];
                                }
                            }
                        },
                    },
                    {
                            extend: 'csv',
                            text: '<i title="export to csv" class="fa fa-file-text-o"></i>',
                            exportOptions: {
                                columns: ':visible:Not(.not-exported)',
                                rows: ':visible'
                            },
                        },
                        {
                            extend: 'print',
                            text: '<i title="print" class="fa fa-print"></i>',
                            exportOptions: {
                                columns: ':visible:Not(.not-exported)',
                                rows: ':visible'
                            },
                        },
                        {
                            extend: 'colvis',
                            text: '<i title="column visibility" class="fa fa-eye"></i>',
                            columns: ':gt(0)'
                        },
                ],
            });

        })(jQuery);
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.client', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Projects\Laravel\peoplepro\resources\views/client/project_status.blade.php ENDPATH**/ ?>