<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		User::insert([[
			'username'=>'admin',
			'email'=>'admin@test.com',
			'password'=> bcrypt('admin1234'),
			'role_users_id'=>1,
			'contact_no'=> 1234
		],
			[
				'username'=>'rony',
				'email'=>'rony@gmail.com',
				'password'=> bcrypt('admin'),
				'role_users_id'=>2,
				'contact_no'=> 1235
			],
			[
			'username' =>'adnan',
			'email'=>'adnan@gmail.com',
			'password'=> bcrypt('admin'),
			'role_users_id'=>2,
			'contact_no'=> 1236
		]]
		);
    }
}
